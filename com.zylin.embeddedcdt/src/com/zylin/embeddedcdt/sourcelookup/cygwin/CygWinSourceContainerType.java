package com.zylin.embeddedcdt.sourcelookup.cygwin;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.sourcelookup.ISourceContainer;

import com.zylin.embeddedcdt.sourcelookup.SourceContainerType;

public class CygWinSourceContainerType extends SourceContainerType
{
	public ISourceContainer createSourceContainer( String memento ) throws CoreException {
		return new CygWinSourceContainer("Cygwin source container");
	}

}
