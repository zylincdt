package com.zylin.embeddedcdt.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PrefConstants {

	public static final String P_PATH = "pathPreference";
	public static final String P_DEBUGGER_NAME = "debuggerName";
	public static final String P_DEBUGGER_INIT = "debuggerInit";
	public static final String P_RES_DEBUGGER_NAME = "ECDT_Debugger_Name" ;
	public static final String P_RES_DEBUGGER_INIT = "ECDT_Debugger_Init" ;
	public static final String P_RES_DESCRIPTION = "ECDT_Main_PrefPage_Desc" ;
}
